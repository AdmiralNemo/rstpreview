from . import routes
import milla


def make_app():
    app = milla.Application(routes.router)
    app.config.setdefault('milla.static_root', 'static/')
    return app
