from . import controllers
from milla.dispatch import routing


router = routing.Router()

router.add_route('/', controllers.IndexController())
router.add_route('/preview', controllers.PreviewController())
