from . import markup
import jinja2
import milla.controllers


class BaseController(milla.controllers.Controller):

    TMPL_LOADER = jinja2.PackageLoader(__name__.rsplit('.', 1)[0])

    def __before__(self, request):
        super(BaseController, self).__before__(request)
        env = jinja2.Environment(loader=self.TMPL_LOADER)
        env.globals.update(
            url=request.create_href,
            static=request.static_resource,
        )
        self.render = lambda t, **k: env.get_template(t).render(**k)

    def __after__(self, request):
        super(BaseController, self).__after__(request)
        del self.render


class IndexController(BaseController):

    allowed_methods = ('GET', 'HEAD', 'POST')

    def __call__(self, request):
        return getattr(self, request.method)(request)

    def GET(self, request):
        response = request.ResponseClass()
        response.text = self.render('index.html.j2')
        return response

    def POST(self, request):
        response = request.ResponseClass()
        try:
            content = request.POST['content']
            preview = markup.get_previewer(content).preview()
        except KeyError:
            content = preview = ''
        response.text = self.render('index.html.j2', **dict(
            content=content,
            preview=preview,
        ))
        return response


class PreviewController(BaseController):

    allowed_methods = ('HEAD', 'POST')

    def __call__(self, request):
        return getattr(self, request.method)(request)

    def POST(self, request):
        response = request.ResponseClass()
        previewer = markup.get_previewer(request.text)
        response.text = previewer.preview()
        return response
