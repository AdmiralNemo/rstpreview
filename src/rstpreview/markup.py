import markupsafe
try:
    import docutils.core
except ImportError:
    docutils = None
try:
    import markdown
except ImportError:
    markdown = None
else:
    import markdown.extensions.codehilite


def get_previewer(source):
    lines = source.splitlines()
    for previewer in PreviewerMeta.previewers:
        if previewer.detect(lines):
            return previewer(source)
    return DefaultPreviewer(source)


class PreviewerMeta(type):

    previewers = []

    def __new__(mcs, name, bases, attrs):
        cls = type.__new__(mcs, name, bases, attrs)
        if hasattr(cls, 'detect'):
            mcs.previewers.append(cls)
        return cls


Previewer = PreviewerMeta('Previewer', (), {})


class BasePreviewer(Previewer):

    def __init__(self, source):
        self.source = source


class ReStructuredTextPreviewer(BasePreviewer):
    DOCUTILS_SETTINGS = {
        'syntax_highlight': 'short',
    }

    @classmethod
    def detect(cls, lines):
        if docutils is None:
            return False
        for idx, line in enumerate(lines):
            if line.startswith('.. '):  # directives, comments
                return True
            if line.endswith('::'):  # directive, literal block
                return True
            if '``' in line:  # default role
                return True
            if ':`' in line:  # role
                return True

    def preview(self):
        return docutils.core.publish_parts(
            source=self.source,
            writer_name='html4css1',
            settings_overrides=self.DOCUTILS_SETTINGS,
        )['html_body']


class MarkdownPreviewer(BasePreviewer):

    def __init__(self, source):
        super(MarkdownPreviewer, self).__init__(source)
        self.extensions = [
            markdown.extensions.codehilite.CodeHiliteExtension(
                css_class='code',
            ),
        ]

    @classmethod
    def detect(cls, lines):
        if markdown is None:
            return False
        for idx, line in enumerate(lines):
            if line.lstrip().startswith('#'):  # heading
                return True
            if '![' in line:  # image
                return True
            if '](' in line:  # inline link
                return True
            if line.startswith('[') and ']:' in line:  # reference-style link
                return True
            if line.startswith('    '):  # code block
                # reStructuredText has a similar syntax, so make sure
                # it isn't that
                if idx and not line[idx - 1].endswith('::'):
                    return True

    def preview(self):
        return markdown.markdown(self.source, extensions=self.extensions)


class PlainTextPreviewer(BasePreviewer):

    def preview(self):
        return '<pre>{0}</pre>'.format(markupsafe.escape(self.source))


if docutils is not None:
    DefaultPreviewer = ReStructuredTextPreviewer
else:
    DefaultPreviewer = PlainTextPreviewer
