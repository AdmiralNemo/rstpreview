from setuptools import find_packages, setup

setup(
    name='RstPreview',
    version='0.1',
    description='Simple browser-based lightweight markup language previewer',
    author='Dustin C. Hatch',
    author_email='dustin@hatch.name',
    url='https://bitbucket.org/AdmiralNemo/rstpreview',
    license='APACHE-2',
    packages=find_packages('src'),
    package_dir={'': 'src'},
)
