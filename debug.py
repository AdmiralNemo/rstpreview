from werkzeug import serving
import rstpreview
import os.path


app = rstpreview.make_app()


try:
    serving.run_simple(
        hostname='::',
        port=8080,
        application=app,
        static_files={
            '/static': os.path.join(os.path.dirname(__file__), 'static'),
        },
    )
except KeyboardInterrupt:
    print('Exiting...')
